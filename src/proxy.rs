use crate::request::{AsyncRequestProxy, RequestProxy};
use std::collections::HashMap;
use zbus::{dbus_proxy, fdo::Result};
use zvariant::derive::{DeserializeDict, SerializeDict, TypeDict};

#[derive(SerializeDict, DeserializeDict, Debug, Clone, Copy, PartialEq, TypeDict)]
/// A response to a pick color request.
/// **Note** the values are normalized.
pub struct Color {
    color: ([f64; 3]),
}

#[dbus_proxy(
    interface = "org.freedesktop.portal.Screenshot",
    default_service = "org.freedesktop.portal.Desktop",
    default_path = "/org/freedesktop/portal/desktop"
)]
trait Screenshot {
    #[dbus_proxy(object = "Request")]
    fn pick_color(&self, parent_window: &str, options: HashMap<&str, zvariant::Value<'_>>);

    /// The version of this DBus interface.
    #[dbus_proxy(property, name = "version")]
    fn version(&self) -> Result<u32>;
}
