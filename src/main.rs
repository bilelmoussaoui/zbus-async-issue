use futures::lock::Mutex;
use gtk::glib;
use gtk::prelude::*;
use std::{collections::HashMap, env::args, sync::Arc};

mod proxy;
mod request;

use proxy::{AsyncScreenshotProxy, Color};
use request::Response;

pub async fn pick_color() -> zbus::Result<Response<Color>> {
    let connection = zbus::azync::Connection::new_session().await?;
    let proxy = AsyncScreenshotProxy::new(&connection)?;
    let request = proxy.pick_color("", HashMap::new()).await?;

    let (sender, receiver) = futures::channel::oneshot::channel();

    let sender = Arc::new(Mutex::new(Some(sender)));
    request
        .connect_response(move |response: Response<Color>| {
            println!("{:#?}", response);
            let s = sender.clone();
            Box::pin(async move {
                if let Some(m) = s.lock().await.take() {
                    let _result = m.send(response);
                }
                Ok(())
            })
        })
        .await
        .unwrap();

    request.next_signal().await?;
    let color = receiver.await.unwrap();
    Ok(color)
}

fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);
    window.set_title("Color Picker");
    window.set_default_size(350, 100);
    let button = gtk::Button::with_label("Click me!");

    button.connect_clicked(move |_| {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            let response = pick_color().await;
            println!("{:#?}", response);
        });
    });

    window.add(&button);

    window.show_all();
}

fn main() {
    let application = gtk::Application::new(Some("com.belmoussaoui.test"), Default::default())
        .expect("Initialization failed...");

    application.connect_activate(build_ui);

    application.run(&args().collect::<Vec<_>>());
}
